﻿using ApiCommon;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using Xunit;

namespace UnitTests
{
    public class PublicKeyProviderTests
    {
        [Fact]
        public async void CanParseEndpoints()
        {
            var endpoint = "https://sqlite-user-api.azurewebsites.net/.well-known/openid-configuration";
            var provider = new PublicKeyProvider();
            await provider.ParseDiscoveryEndpoints(new List<String> { endpoint });
            var numKeys = provider.GetNumberOfKeys();
            Assert.Equal(1, numKeys);

        }

        [Fact]
        public async void CanParseEndpointWithTrailingSlash()
        {
            var endpoint = "https://sqlite-user-api.azurewebsites.net/.well-known/openid-configuration/";
            var provider = new PublicKeyProvider();
            await provider.ParseDiscoveryEndpoints(new List<String> { endpoint });
            var numKeys = provider.GetNumberOfKeys();
            Assert.Equal(1, numKeys);
        }

        [Fact]
        public async void ThrowsExceptionOnInvalidEndpoint()
        {
            var exceptionThrown = false;
            try
            {
                var endpoint = "https://www.google.com";
                var provider = new PublicKeyProvider();
                await provider.ParseDiscoveryEndpoints(new List<String> { endpoint });
                
            }catch(Exception e)
            {
                exceptionThrown = true;
            }

            Assert.True(exceptionThrown);
        }
    }
}

﻿using ApiCommon;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using Xunit;

namespace UnitTests
{
    public class EncrypterTests
    {
        [Fact]
        public void CanEncrypt()
        {
            RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
            var privateXmlString = rsa.ToXmlString(true);
            var publicXmlString = rsa.ToXmlString(false);

            var encrypter = new Encrypter(privateXmlString, publicXmlString);
            var issuer = "issuer";
            var username = "username";
            var token = encrypter.CreateToken(username, issuer);
            //Find the issuer and kid
            var split = token.Split('.');
            var payloadString = Base64Decode(split[1]);
            //Get payload
            var payload = JsonConvert.DeserializeObject<JWTokenPayload>(payloadString);

            Assert.Equal(issuer, payload.iss);

        }

        private String Base64Decode(String base64)
        {
            var base64Encoded = base64.PadRight(base64.Length + (4 - base64.Length % 4) % 4, '=');
            byte[] data = System.Convert.FromBase64String(base64Encoded);
            return System.Text.ASCIIEncoding.ASCII.GetString(data);
        }

    }
}

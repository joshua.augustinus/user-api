﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using ApiCommon;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using UserApi.DataObjects;

namespace UserApi.Controllers
{
    
    [ApiController]
    public class DiscoveryDocumentController : ControllerBase
    {
        private DiscoveryDocument Document { get;set;}
        public DiscoveryDocumentController(IOptions<DiscoveryDocument> configuration)
        {
            Document = configuration.Value;
        }

        [Route(".well-known/openid-configuration")]
        [HttpGet]
        public object GetDocument()
        {
            if (Debugger.IsAttached)
            {
                Document.issuer = "https://localhost:44344/";
                Document.jwks_uri = "https://localhost:44344/jwks";
            }
            return Document;
        }
    }
}

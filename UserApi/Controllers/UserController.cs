﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiCommon;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Razor.Internal;
using Microsoft.Extensions.Options;
using UserApi.DataObjects;
using UserApi.Helpers;

namespace UserApi.Controllers
{
    [Authorize(AuthenticationSchemes = "Bearer")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private DbHelper _dbHelper;
        private Encrypter _encrypter;
        private String _issuer;

        public UserController(DbHelper dbHelper, Encrypter encrypter, IOptions<DiscoveryDocument> configuration)
        {
            _dbHelper = dbHelper;
            _encrypter = encrypter;
            _issuer = configuration.Value.issuer;

        }

        [HttpGet]
        [Route("user/{username}")]
        public object GetUser(String username )
        {
            if (username == null)
                return BadRequest("Username is null");

            var result = _dbHelper.GetUser(username);
            return Ok(result);

        }


        [HttpPost]
        [Route("user")]
        public object CreateUser(CreateUserRequest request)
        {
            if (request == null)
                return BadRequest("Request is null");

            var result = _dbHelper.CreateUser(request.Username, request.Password);
            if (!result)
                return BadRequest("Username exists");
            else
            {
                var response = new AccessTokenResponse(_encrypter.CreateToken(request.Username, _issuer), request.DisplayName);

                return Ok(response);
            }

        }


        [Authorize(Policy = "SameUserNamePolicy")]
        [HttpPut]
        [Route("user/{username}")]
        public object UpdateUSer(String username, CreateUserRequest request)
        {
            if (String.IsNullOrEmpty(username))
            {
                return BadRequest("Username cannot be null");
            }
            if (request == null)
            {
                return BadRequest("Request payload cannot be null");
            }

            request.Username = username;
            if (!String.IsNullOrEmpty(request.Password))
            {
                 _dbHelper.UpdateUserPassword(request.Username, request.Password);
            }

            if(!String.IsNullOrEmpty(request.DisplayName))
            {
                _dbHelper.UpdateDisplayName(request.Username, request.DisplayName);
            }


            return Ok();
        }
    }
}

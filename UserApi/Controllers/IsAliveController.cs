﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiCommon;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using UserApi.Helpers;

namespace UserApi.Controllers
{
    [ApiController]
    public class IsAliveController : ControllerBase
    {
        private readonly DbHelper _dbHelper;
        private IPublicKeyProvider _keyProvider;
        public IsAliveController(DbHelper dbHelper, IPublicKeyProvider provider)
        {
            _dbHelper = dbHelper;
            _keyProvider = provider;
        }

      
        [HttpGet]
        [Route("isalive")]
        public object IsAlive()
        {
            _dbHelper.TestConnection();

            var keysLoaded = _keyProvider.GetNumberOfKeys();
            return new { publicKeysLoaded = keysLoaded};
        }
    }
}

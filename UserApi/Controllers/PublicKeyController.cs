﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiCommon;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace UserApi.Controllers
{
    /// <summary>
    /// This controller exposes the XML public key as a base 64 string. If you want to use the standard
    /// then use the jwks endpoint instead.
    /// </summary>
    [ApiController]
    public class PublicKeyController : ControllerBase
    {
        private Encrypter _encrypter;
        public PublicKeyController(Encrypter encrypter)
        {
            _encrypter = encrypter;
        }
        [HttpGet]
        [Route("publickey")]
        public object GetPublicKey()
        {
            return new { publicKey = _encrypter.GetPublicKey() };
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiCommon;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using UserApi.DataObjects;
using UserApi.Helpers;

namespace UserApi.Controllers
{

    [ApiController]
    public class TokenController : ControllerBase
    {
        private DbHelper _dbHelper;
        private Encrypter _encrypter;
        private String _issuer;
        public TokenController(DbHelper dbHelper, Encrypter encrypter, IOptions<DiscoveryDocument> options)
        {
            _dbHelper = dbHelper;
            _encrypter = encrypter;
            _issuer = options.Value.issuer;
        }
        [HttpPost]
        [Route("token")]
        public object CreateToken(CreateUserRequest request)
        {
            var patientDTO = _dbHelper.ValidatePassword(request.Username, request.Password);
            if (patientDTO !=null)
            {
                var token = _encrypter.CreateToken(request.Username, _issuer);
                var response = new AccessTokenResponse(token, patientDTO.DisplayName);
                return Ok(response);
            }
            else
            {
                return Unauthorized();
            }
        }
    }
}

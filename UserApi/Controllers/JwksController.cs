﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiCommon;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace UserApi.Controllers
{
    /// <summary>
    /// This endpoint needs to return something like: https://auth0.com/docs/tokens/references/jwks-properties
    /// In the example they provide x5t, x5c, and kid, but these fields are optional
    /// Source: https://tools.ietf.org/html/rfc7517#section-4.4
    /// </summary>
    [ApiController]
    public class JwksController : ControllerBase
    {
        private Encrypter _encrypter;
        public JwksController(Encrypter encrypter)
        {
            _encrypter = encrypter;
        }

        [Route("jwks")]
        [HttpGet]
        public object GetJsonKeySet()
        {
            return _encrypter.GetJsonWebKeySet();
        }

    }
}

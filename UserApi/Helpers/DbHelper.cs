﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Data.Sqlite;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using UserApi.DataObjects;

namespace UserApi.Helpers
{
    public class DbHelper
    {
        private const String DATETIME_FORMAT = "o";
        private const String CONN_STRING = "Data Source=fcm.db";
        private IWebHostEnvironment _webHostEnvironment;
        public DbHelper(IWebHostEnvironment webHostEnvironment)
        {
            _webHostEnvironment = webHostEnvironment;
        }

        private static SqliteConnection CreateConnection()
        {
            return  new SqliteConnection(CONN_STRING);
        }
        public  void TestConnection( )
        {
            var stm = "SELECT SQLITE_VERSION()";

            using (var con = new SqliteConnection(CONN_STRING))
            {
                con.Open();

                using (var cmd = new SqliteCommand(stm, con))
                {
                    string version = cmd.ExecuteScalar().ToString();
                }


            }
        }

        private static String GetCurrentTimeString()
        {
            return DateTime.UtcNow.ToString(DATETIME_FORMAT);
        }

        private static DateTime ParseDateTimeString(String dateTime)
        {
            return DateTime.ParseExact(dateTime, DATETIME_FORMAT, CultureInfo.InvariantCulture);
        }

        public bool UpdateUserPassword(String username, String password)
        {
            if (!UsernameExists(username))
            {
                return false;
            }

            var query = @"UPDATE [User] SET hashedpassword = @hashedpassword WHERE username=@username";
            using (var con = CreateConnection())
            {
                con.Open();

                using (var cmd = new SqliteCommand(query, con))
                {
                    cmd.Parameters.AddWithValue("@username", username);
                    cmd.Parameters.AddWithValue("@hashedpassword", PasswordHelper.HashPassword(password));

                    cmd.ExecuteNonQuery();
                }


            }

            return true;
        }

        public bool UpdateDisplayName(String username, String displayName)
        {
            if (!UsernameExists(username))
            {
                return false;
            }

            var query = @"UPDATE [User] SET displayName = @displayname WHERE username=@username";
            using (var con = CreateConnection())
            {
                con.Open();

                using (var cmd = new SqliteCommand(query, con))
                {
                    cmd.Parameters.AddWithValue("@username", username);
                    cmd.Parameters.AddWithValue("@displayname", displayName);

                    cmd.ExecuteNonQuery();
                }


            }

            return true;
        }

        /// <summary>
        /// Creates a new user
        /// </summary>
        /// <returns>False if username exists</returns>
        public bool CreateUser(String username, String password)
        {
            if (UsernameExists(username))
            {
                return false;
            }

            var query = @"insert into User (username, hashedpassword, adddatetime)
                values (@username, @password, @datetime)";
            using (var con = CreateConnection())
            {
                con.Open();

                using (var cmd = new SqliteCommand(query, con))
                {
                    cmd.Parameters.AddWithValue("@username", username);
                    cmd.Parameters.AddWithValue("@password", PasswordHelper.HashPassword(password));
                    cmd.Parameters.AddWithValue("@datetime", GetCurrentTimeString());
                    cmd.ExecuteNonQuery();
                }


            }

            return true;
        }

        /// <summary>
        /// Creates a new user
        /// </summary>
        /// <returns>False if username exists</returns>
        public UserDTO GetUser(String username)
        {
            var query = @"select DisplayName from User where username=@username limit 1";
            using (var con = CreateConnection())
            {
                con.Open();

                using (var cmd = new SqliteCommand(query, con))
                {
                    cmd.Parameters.AddWithValue("@username", username);
                  
                    using(var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var displayName = reader.GetValue(0) as String;
                            return new UserDTO { Username = username, DisplayName = displayName };
                        }

                      
                    }
                }


            }

            return null;
        }

        public bool UsernameExists(String username)
        {
            var query = @"select count(*) from [User] where Username = @username";
            using (var con = CreateConnection())
            {
                con.Open();

                using (var cmd = new SqliteCommand(query, con))
                {
                    cmd.Parameters.AddWithValue("@username", username);
                    var result = Convert.ToInt32(cmd.ExecuteScalar());
                    return result > 0;
                }


            }


        }


        public UserDTO ValidatePassword(String username, String password)
        {
            var query = @"select hashedpassword, displayName from [User] where Username = @username";
            using (var con = CreateConnection())
            {
                con.Open();

                using (var cmd = new SqliteCommand(query, con))
                {
                    cmd.Parameters.AddWithValue("@username", username);


                    using(var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var hashedPassword = reader.GetValue(0) as string;
                            var displayName = reader.GetValue(1) as string;
                            var isValid = PasswordHelper.ValidatePassword(password, hashedPassword.ToString());
                            return new UserDTO { DisplayName = displayName, Username = username };
                        }
                    }

                  
                }
            }

            return null;
        }

    }
}

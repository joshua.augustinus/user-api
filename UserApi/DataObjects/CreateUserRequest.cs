﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UserApi.DataObjects
{
    public class CreateUserRequest
    {
        public String Username { get; set; }
        public String Password { get; set; }
        public String DisplayName { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UserApi.DataObjects
{
    public class AccessTokenResponse
    {
        public AccessTokenResponse(String accessToken, String displayName)
        {
            access_token = accessToken;
            token_type = "bearer";
            DisplayName = displayName;
        }

        public String access_token { get; set; }
        public String token_type { get; set; }

        public String DisplayName { get; set; }
    }
}

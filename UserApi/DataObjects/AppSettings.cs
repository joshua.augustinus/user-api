﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UserApi.DataObjects
{
    public class AppSettings
    {
        public String JwksUri { get; set; }
        public String Issuer { get; set; }
    }
}

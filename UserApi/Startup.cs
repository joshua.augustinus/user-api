using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;
using ApiCommon;
using ApiCommon.Filters;
using Jose;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Server.IISIntegration;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using UserApi.DataObjects;
using UserApi.Helpers;

namespace UserApi
{
    public class Startup
    {
        private const String CORS_POLICY_NAME = "MyPolicy";
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            string path = Path.Combine(Directory.GetCurrentDirectory(), "App_Data");
            AppDomain.CurrentDomain.SetData("DataDirectory", path);

        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //Required for debugging locally
            var allowedOrigins = Configuration.GetSection("AllowedOrigins").GetChildren().Select(x => x.Value).ToArray();
            //For some reason this won't work if I allow all origins
            services.AddCors(options =>
            {
                options.AddPolicy(name: CORS_POLICY_NAME,
                                  builder =>
                                  {
                                      builder.WithOrigins(allowedOrigins).AllowAnyMethod().AllowAnyHeader().AllowCredentials();
                                  });
            });
            services.AddControllers();
            services.AddSingleton<IAuthorizationHandler, CustomAuthHandler>();
            services.AddAuthorization(options =>
            {
                options.AddPolicy("SameUserNamePolicy", policy =>
                    policy.Requirements.Add(new CustomRequirement { CheckUsernameRouteValue =true}));
            });
            services.AddHttpContextAccessor();//Required for using  IHttpContextAccessor
            services.AddSingleton<DbHelper>();
            services.AddSingleton<Encrypter>();
            services.AddSingleton<Decrypter>();
            services.AddSingleton<IPublicKeyProvider, PublicKeyProvider>();
            //For other apis use AuthApiClient as the provider
            //services.AddHttpClient<IPublicKeyProvider, AuthApiClient>();
            services.AddAuthentication()
                .AddScheme<BasicAuthenticationOptions, BasicAuthenticationHandler>("Basic", null);
            services.AddAuthentication().AddScheme<BearerAuthenticationOptions, BearerAuthenticationHandler>("Bearer", null);
            services.Configure<DiscoveryDocument>(Configuration.GetSection("DiscoveryDocument"));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseCors(CORS_POLICY_NAME);
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            //Activate constructors of our singletons
            var encrypter = app.ApplicationServices.GetRequiredService<Encrypter>();

            //Get the discovery endpoints and them parse them to get public keys
            var discoveryEndpoints = Configuration.GetSection("DiscoveryEndpoints").GetChildren().Select(x => x.Value).ToArray();
            var publicKeyProvider = app.ApplicationServices.GetRequiredService<IPublicKeyProvider>();
            publicKeyProvider.ParseDiscoveryEndpoints(discoveryEndpoints);

        }
    }
}

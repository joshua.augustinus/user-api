﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ApiCommon
{
    public interface IPublicKeyProvider
    {
        JWKSItem GetPublicKey(String accessToken);

        Task ParseDiscoveryEndpoints(IEnumerable<String> endpoints);


        int GetNumberOfKeys();
   
    }
}

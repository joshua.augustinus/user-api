﻿using Jose;

using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace ApiCommon
{
    /// <summary>
    /// https://stackoverflow.com/questions/38794670/how-to-create-encrypted-jwt-in-c-sharp-using-rs256-with-rsa-private-key
    /// </summary>
    public class Encrypter
    {
        //key size for RS256; this is recommended; theoretically you could use a different key size
        //smaller key sizes are easier to crack
        private const int KEY_SIZE_BITS = 2048;
        private String _publicXmlString;
        private String _privateXmlString;
        private String PRIVATE_KEYPATH = $"App_Data/privatekey.xml";
        private String PUBLIC_KEYPATH = $"App_Data/publickey.xml";

        public Encrypter()
        {
            PrepareKeys();
        }


        /**
         * Inititalise the Encrypter class with your own exponent and modulus
         * privateXmlString: Created from RSACryptoServiceProvider 
         * publicXmlString: Created from RSACryptoServiceProvider
         */
        public Encrypter(String privateXmlString, String publicXmlString)
        {
            _publicXmlString = publicXmlString;
            _privateXmlString = privateXmlString;
        }

        public String CreateToken(JWTokenPayload payload)
        {
            if (String.IsNullOrEmpty(_publicXmlString))
                throw new Exception("Call PrepareKeys first");
            RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
            rsa.FromXmlString(_privateXmlString);

            return JWT.Encode(payload, rsa, Jose.JwsAlgorithm.RS256);

        }

        public JsonWebKeySet GetJsonWebKeySet()
        {
            RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
            rsa.FromXmlString(_publicXmlString);
            var parameters = rsa.ExportParameters(false);
            var modulus = Convert.ToBase64String(parameters.Modulus);
            var exponent = Convert.ToBase64String(parameters.Exponent);

            var item = new JWKSItem(exponent, modulus);
            return new JsonWebKeySet {keys = new JWKSItem[] { item } };
        }

        /// <summary>
        /// Encrypts a JWTokenPayload
        /// </summary>
        /// <param name="username"></param>
        /// <param name="issuer">e.g. https://accounts.google.com</param>
        /// <returns></returns>
        public String CreateToken(String username, String issuer)
        {
            var payload = new JWTokenPayload { sub = username, iss=issuer };
            return CreateToken(payload);
        }

        /// <summary>
        /// Returns the public key as a base 64 string. Original is in XML format
        /// </summary>
        /// <returns></returns>
        public String GetPublicKey()
        {
            return Base64Encode(_publicXmlString);
        }

        public string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }

        public void PrepareKeys()
        {
            if(File.Exists(PUBLIC_KEYPATH)){
                //Instantiate the RSACryptoServiceProvider using keys already saved
                RSACryptoServiceProvider rsa = new RSACryptoServiceProvider();
                var xml = File.ReadAllText(PRIVATE_KEYPATH);
                rsa.FromXmlString(xml);
                _privateXmlString = rsa.ToXmlString(true);
                _publicXmlString = rsa.ToXmlString(false);
            }
            else
            {
                //Prepare keys for first time
                RSACryptoServiceProvider rsa = new RSACryptoServiceProvider(KEY_SIZE_BITS);

                //Create the pub and private key
                string pubkey = rsa.ToXmlString(false);
                string prikey = rsa.ToXmlString(true);

                _publicXmlString = pubkey;
                _privateXmlString = prikey;

                //Now save 
                File.WriteAllText(PRIVATE_KEYPATH, _privateXmlString);
                File.WriteAllText(PUBLIC_KEYPATH, _publicXmlString);
            }
        }


    }
}

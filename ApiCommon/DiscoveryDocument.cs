﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ApiCommon
{
    /// <summary>
    /// https://openid.net/specs/openid-connect-discovery-1_0.html#ProviderMetadata
    /// </summary>
    public class DiscoveryDocument
    {
        public String issuer { get; set; }
        public String authorization_endpoint { get; set; }
        public String token_endpoint { get; set; }
        public String jwks_uri { get; set; }
        public String[] response_types_supported { get; set; }
        public String[] id_token_signing_alg_values_supported { get; set; }


        public DiscoveryDocument()
        {
        }

    }
}

﻿using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Text;

namespace ApiCommon.Filters
{
    public class CustomRequirement: IAuthorizationRequirement
    {
        public bool CheckUsernameRouteValue { get; set; }
    }
}

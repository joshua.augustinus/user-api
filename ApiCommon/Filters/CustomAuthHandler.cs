﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authorization.Infrastructure;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Security.Claims;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Routing;

namespace ApiCommon.Filters
{
    /**
     * 
    */
    public class CustomAuthHandler : AuthorizationHandler<CustomRequirement>
    {
        IHttpContextAccessor _httpContextAccessor;

        public CustomAuthHandler( IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, CustomRequirement requirement)
        {
            var passedAllChecks = true;
            if (requirement.CheckUsernameRouteValue)
            {
                //Requires extension Microsoft.AspNetCore.Routing
                var usernameRouteValue = _httpContextAccessor.HttpContext.GetRouteValue("username");

                if (usernameRouteValue != null)
                {
                    if (usernameRouteValue.ToString() != context.User.Identity.Name)
                    {
                        passedAllChecks = false;
                    }
                }
            }


            if(passedAllChecks)
                context.Succeed(requirement);


            return Task.CompletedTask;
        }
    }
}

﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;

namespace ApiCommon.Filters
{
    /**
     * This handler is not complete; it needs to be fixed:
     * https://gitlab.com/joshua.augustinus/user-api/-/issues/33
     */
    public class BasicAuthenticationHandler : AuthenticationHandler<BasicAuthenticationOptions>
    {
        public BasicAuthenticationHandler(IOptionsMonitor<BasicAuthenticationOptions> options,
    ILoggerFactory logger,
    UrlEncoder encoder,
    ISystemClock clock)
    : base(options, logger, encoder, clock)
        {

        }
        protected override Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            //Tutorial here:
            //https://joonasw.net/view/creating-auth-scheme-in-aspnet-core-2

            try
            {
                AuthenticationHeaderValue.TryParse(Request.Headers["Authorization"], out AuthenticationHeaderValue headerValue);

                byte[] headerValueBytes = Convert.FromBase64String(headerValue.Parameter);
                string userAndPassword = Encoding.UTF8.GetString(headerValueBytes);
                string[] parts = userAndPassword.Split(':');
                if (parts.Length != 2)
                {
                    throw new Exception();
                }
                string user = parts[0];
                string password = parts[1];

                var claims = new[] { new Claim(ClaimTypes.Name, user) };
                var identity = new ClaimsIdentity(claims, Scheme.Name);
                var principal = new ClaimsPrincipal(identity);

                // Later we can use User.Identity.Name later down the stack.
                var ticket = new AuthenticationTicket(principal, Scheme.Name);
                return Task.FromResult(AuthenticateResult.Success(ticket));
            }catch(Exception e)
            {
                return Task.FromResult(AuthenticateResult.Fail("Invalid Basic authentication header"));
            }

        }
    }
}

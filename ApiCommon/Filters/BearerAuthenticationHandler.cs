﻿using ApiCommon;
using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text.Encodings.Web;
using System.Threading.Tasks;

namespace ApiCommon.Filters
{
    public class BearerAuthenticationHandler : AuthenticationHandler<BearerAuthenticationOptions>
    {
        private Decrypter _decrypter;
        public BearerAuthenticationHandler(IOptionsMonitor<BearerAuthenticationOptions> options,
    ILoggerFactory logger,
    UrlEncoder encoder,
    ISystemClock clock, Decrypter decrypter)
    : base(options, logger, encoder, clock)
        {
            _decrypter = decrypter;
        }

    protected override Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            if (!Request.Headers.ContainsKey("Authorization"))
            {
                //Authorization header not in request
                return Task.FromResult(AuthenticateResult.NoResult());
            }

            if (!AuthenticationHeaderValue.TryParse(Request.Headers["Authorization"], out AuthenticationHeaderValue headerValue))
            {
                //Invalid Authorization header
                return Task.FromResult(AuthenticateResult.NoResult());
            }

            if (!"Bearer".Equals(headerValue.Scheme, StringComparison.OrdinalIgnoreCase))
            {
                return Task.FromResult(AuthenticateResult.NoResult());
            }

            try
            {
                var token = headerValue.Parameter;
                var payload = _decrypter.DecodeToken(token);
                var claims = new[] { new Claim(ClaimTypes.Name, payload.sub)};
                var identity = new ClaimsIdentity(claims, Scheme.Name);
                var principal = new ClaimsPrincipal(identity);

                // Later we can use User.Identity.Name later down the stack.
                var ticket = new AuthenticationTicket(principal, Scheme.Name);

       
                return Task.FromResult(AuthenticateResult.Success(ticket));

            }catch(Exception e)
            {
                return Task.FromResult(AuthenticateResult.NoResult());
            }

        }
    }
}
